
import matplotlib.pyplot as plt
from matplotlib.pyplot import MultipleLocator
import numpy as np
import math
import sys,os

class Astar:
    
    def __init__(self,width,height,gridSize,robot_location,target_location):
    # ��ʼ��
        self.Width = width  # ��ȵ�դ����
        self.Height = height
        self.gridSize = gridSize    # դ��Ĵ�С
        self.grid = None    # դ��ռ� ���� np.zeros(width,height)
        self.obs = None     # �ϰ�������
        self.robotLoc = robot_location  # ��������ʼ��դ���λ��
        self.targetLoc = target_location    # Ŀ���λ
        self.ENABLE_DRAW = True     # �Ƿ�������ƶ�ͼ
        self.creatGrid()    # ����դ��ռ�
        self.creatObstacle()    # �����ϰ������꣬������
        self.writeObstacle()    # 
        self.planning() # �滮�켣
        
        if self.ENABLE_DRAW:
            plt.show()
        
    
    def creatGrid(self):
        # ����դ��
        self.grid = np.zeros(((int)(self.Height), (int)(self.Width)))   # ��ʼ��դ��ռ�
        plt.axis('scaled')  # ���������С�̶�һ��     
        axis = plt.gca()    # ��ȡ������ĵ�
        xAxis = np.arange(0,(self.Width+1)*self.gridSize,self.gridSize) # ����դ��
        yAxis = np.arange(0,(self.Height+1)*self.gridSize,self.gridSize)
        axis.set_xticks(xAxis)
        axis.set_yticks(yAxis)
        axis.tick_params(axis = 'both', direction = 'in')
        plt.grid(True)  # ����դ��

        # ���ƻ��������� �� �յ�����
        self.drawPoint(self.robotLoc,markersize = 6, marker = 'o', markerfacecolor = 'red', markeredgecolor = 'red')
        self.drawPoint(self.targetLoc,markersize = 6, marker = 'v', markerfacecolor = 'green', markeredgecolor = 'green')
        


    def creatObstacle(self):
        # �����ϰ��� 
        self.obs = []

        
        obs1 = [(0,a) for a in range((int)(self.Height))]
        obs2 = [((int)(self.Width) - 1,a) for a in range((int)(self.Height))]
        obs3 = [(a,0) for a in range((int)(self.Width))]
        obs4 = [(a,(int)(self.Height) - 1) for a in range((int)(self.Width))]
        obs5 = [(13,a) for a in range(20)]
        obs6 = [(a,b) for a in range(18,22,1) for b in range(16,28,1)]
        obs7 = [(a,b) for a in range(24,26,1) for b in range(28,32,1)]
        obs8 = [(a,b) for a in range(30,32,1) for b in range(26,39,1)]
        # �����ϰ���
        self.obs = self.obs + obs1 + obs2 + obs3 + obs4 + obs5 + obs6  + obs7 + obs8


        for item in list(self.obs):
            self.drawPoint(item,markersize = 4, marker = 's', markerfacecolor = 'black', markeredgecolor = 'black')

    def writeObstacle(self):
        # ���ϰ��� д�� դ����ͬ�������� 1��
        print(self.grid)
        for item in list(self.obs):
            self.grid[item[0],item[1]] = 1;
        print(self.grid)

    def drawPoint(self,loction,markersize = 4,marker = 's',markerfacecolor = 'black',markeredgecolor = 'black'):
        # ������ƽ�դ����Ϊդ���ÿ̶ȱ�ʾ�����Ի��Ƶ�ʱ��Ҫ����ƫ�ã�
        plt.plot(loction[0]*self.gridSize + self.gridSize/2,loction[1]*self.gridSize + self.gridSize/2,
                 linestyle = '',
                 markersize = markersize,
                 marker = marker,
                 markerfacecolor = markerfacecolor,
                 markeredgecolor = markeredgecolor)
        

    class Node:
        # �ڵ���   
        def __init__(self,pos,cost,parent_pos):
            self.pos = pos
            self.cost = cost
            self.parent_pos = parent_pos

        def __str__(self):
            return str(self.pos) + "," + str(
                self.cost) + "," + str(self.parent_index)

    def hevristic(self,pos):
        # ���ʽ�������˺�����������յ㵽�����ľ���
        distance = math.hypot(pos[0] - self.targetLoc[0],pos[1] - self.targetLoc[1])
        return distance

    def planning(self):
    # ·���滮����
        start_node = self.Node(self.robotLoc,0,[-1,-1])
        end_node = self.Node(self.targetLoc,0,[-1,-1])
        open_set, closed_set = dict(),dict()
        open_set[self.robotLoc] = start_node

        while True:

            minCostPos = min(open_set, key=lambda o: open_set[o].cost + self.hevristic(open_set[o].pos))  # 带启发式的
            # key=lambda o: open_set[o].cost      lambda��һ�����������ǹ̶�д��,�ȼ��� def key(o): return open[o].cost
            # print(minCostPos)
            currentPos = minCostPos    # ��ǰ��¼�ڵ�����С�Ľڵ�
            closed_set[currentPos] = open_set[minCostPos]   # ��¼�Ľڵ�����
            
            del open_set[minCostPos]    # ɾ���ոմ� open_set ����¼�Ľڵ�
                
            if currentPos == self.targetLoc:    # �����ǰ��λ����������յ� ����ѭ��
                goal_node = self.Node(currentPos,closed_set[currentPos].cost,closed_set[currentPos].parent_pos) # ���յĽڵ����ã�Ҳ����ֱ�ӵ���closed_set[currentPos]
                break
            self.findNeiborsAndRecord(currentPos,open_set,closed_set)   # �ҳ���ǰ�Ľڵ���ٽ��ڵ㣬�����¾���

            if self.ENABLE_DRAW:    # ����������
                self.drawPoint(currentPos, marker = 'x', markerfacecolor = 'blue', markeredgecolor = 'blue')    # ���Ƴ���ǰ�Ľڵ�
                if len(closed_set.keys()) % 30 == 0:
                    plt.pause(0.001)

        if self.ENABLE_DRAW:
            rx, ry = self.final_path(goal_node, closed_set)
            plt.plot(rx,ry,color = 'red',linewidth = 2)
    
    # �ҳ���ǰ�ڵ���ٽ��ڵ�
    def findNeiborsAndRecord(self,pos,open_set,closed_set):# pos �ǵ�ǰ�ڵ�
        motion = self.get_motion_model()   # �������ƶ����� ǰ�������ҡ���ǰ�������ǰ���Һ�
        for item in motion:    # ���n���ٽ��ڵ㣬������˵��˶���ʽ�й�
            # print(item[0:2])
            p = (pos[0] + item[0], pos[1] + item[1])    # �ϳ�����

            if p[0] < 0 or p[1] < 0 or self.grid[p[0]][p[1]] == 1 or p in closed_set:   # �����դ���� ���� ���ϰ����� ���� �Ѿ���close_set��¼
                continue
            
            if p not in open_set:   # ����open_set���Ž�open_set ��
                node = self.Node(p,item[2] + closed_set[pos].cost,pos)  # 
                open_set[p] = node
            else:    # �����open_set���棬��Ҫ���¾���
                if open_set[p].cost > item[2] + closed_set[pos].cost:
                      node = self.Node(p,item[2] + closed_set[pos].cost,pos)
                      open_set[p] = node           
        

    def final_path(self, goal_node, closed_set):
    # �������յ�·�������� �ڵ����һ�ڵ� ���б�����Ѱ�ҵ���һ���ڵ�
        a = goal_node
        rx, ry = [],[]
        while a.parent_pos != self.robotLoc:
            rx.append(a.pos[0] * self.gridSize + self.gridSize/2)
            ry.append(a.pos[1] * self.gridSize + self.gridSize/2)
            a = closed_set[a.parent_pos]
        rx.append(self.robotLoc[0] * self.gridSize + self.gridSize/2)
        ry.append(self.robotLoc[1] * self.gridSize + self.gridSize/2)
        return rx, ry   # ���ؽڵ��ֵ


    @staticmethod
    def get_motion_model():
        # dx, dy, cost
        motion = [[1, 0, 1],
                  [0, 1, 1],
                  [-1, 0, 1],
                  [0, -1, 1],
                  [-1, -1, math.sqrt(2)],
                  [-1, 1, math.sqrt(2)],
                  [1, -1, math.sqrt(2)],
                  [1, 1, math.sqrt(2)]]
        return motion

def wait_key():
    ''' Wait for a key press on the console and return it. '''
    result = None
    if os.name == 'nt':
        import msvcrt
        result = msvcrt.getch()
    else:
        import termios
        fd = sys.stdin.fileno()
 
        oldterm = termios.tcgetattr(fd)
        newattr = termios.tcgetattr(fd)
        newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, newattr)
 
        try:
            result = sys.stdin.read(1)
        except IOError:
            pass
        finally:
            termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
 
    return result

def main():
    start_pos = (5,5)
    end_pos = (35,35)
    grid_width = 40
    grid_height = 40
    min_grid = 5
    plt.axis('scaled')  # 横纵坐标大小刻度一致
    axis = plt.gca()    # 获取坐标轴的点
    xAxis = np.arange(0,41*5,5) # 设置栅格
    yAxis = np.arange(0,41*5,5)
    axis.set_xticks(xAxis)
    axis.set_yticks(yAxis)
    axis.tick_params(axis = 'both', direction = 'in')
    plt.grid(True)  # 绘制栅格
    plt.ioff()
    plt.gcf().show()
    # wait_key()
    dijkstar = Astar(grid_width,grid_height,min_grid,start_pos,end_pos)
    
    return
    
if __name__ == '__main__':
    main()